!*****************
program disk
  use disk_mods
  use ifport
  real*8::rp,zp,alpha,ll,theta,t,dt
  integer::i,j,npart

  alpha = 1d-3 !alpha paramter for turbulence
  npart = 300 !number of test particles

  !!WRITE THE CODE HERE TO PLOT THE DENSITY AND
  !! THE TEMPERATURE MAP IN r,z COORDINATES


  !loop on superparticles
  do j=1,npart
     !dump some output
     if(mod(j,npart/10)==0) print *,int(j*1d2/npart),"%"
     !random initialization for superparticles
     rp = 190.*rand()+10. !AU
     zp = 100.*rand()-50. !AU

     t = 0d0 !initial time, s
     !loop on keplerian orbits 
     do i=1,500
        dt = dt_disk(rp,zp) !time-step, s
        t = t + dt !absolute time, s

        !define a random angle
        theta = rand()*2.*pi
        !compute path length, AU
        ll = 2.*sqrt(alpha)*H(rp)
        !update positions, AU
        rp = rp + ll*cos(theta)
        zp = zp + ll*sin(theta)
     end do
  end do

  print *,"done!"
end program
