# this script plots contours from a file with format
#     r  z ngas tgas
# usage is 
# python plot_background.py FILENAME [ngas | tgas] n_r n_z


################################
# NO NEED TO MODIFY HERE BELOW
################################
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
import sys
import os.path
import ipdb

argv = sys.argv

if(len(argv)!=2): sys.exit("ERROR: usage is python "+argv[0]+" FILENAME")
fname = argv[1].strip()

if(not(os.path.isfile(fname))): sys.exit("ERROR: "+fname+" doesn't exist!")

x = np.genfromtxt(fname)
# determine nr, nz
nr = len(set(x[:,0]))
nz = len(set(x[:,1]))
r, z = np.reshape(x[:,0],(nz,nr)).T, np.reshape(x[:,1].T,(nz,nr)).T
ngas, tgas = np.reshape(x[:,2],(nz,nr)).T, np.reshape(x[:,3],(nz,nr)).T

#prepare plot
fig = plt.figure('background model',figsize=(12.0,5.0))
fig.clf()
ax1 = fig.add_subplot(1,2,1)
ax2 = fig.add_subplot(1,2,2)
ax1.set_xlabel('r/AU')
ax1.set_ylabel('z/AU')
ax2.set_xlabel('r/AU')
ax2.set_ylabel('z/AU')
# do tgas plot
fmt = "%g"
levels = [20.,30.,50.,100.,1000.]
CS1 = ax1.contour(r, z, tgas, levels)
ax1.clabel(CS1, inline=1, fontsize=10, fmt=fmt)
ax1.set_title(r"$T_\mathrm{gas}$")

# do ngas plot
fmt = ticker.LogFormatterMathtext()
levels = [1e4,1e6,1e8,1e9]
CS2 = ax2.contour(r, z, ngas, levels)
ax2.clabel(CS2, inline=1, fontsize=10, fmt=fmt)
ax2.set_title(r"$n_\mathrm{gas}$")

plt.show()
