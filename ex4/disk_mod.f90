module disk_mods
  real*8,parameter::H100=16. !AU
  real*8,parameter::hh=1.25
  real*8,parameter::Mgas=.18 !Mo
  real*8,parameter::Rc=81. !AU
  real*8,parameter::g=.75
  real*8,parameter::T0=23. !K
  real*8,parameter::beta=1.5
  real*8,parameter::q=0.5
  real*8,parameter::R0=1d2 !AU
  real*8,parameter::pi=3.1415
  real*8,parameter::mu=1.22
  real*8,parameter::mp=1.67262178d-24 !g
  real*8,parameter::kb_erg=1.3806488d-16
  real*8,parameter::fact3 = 5.94124d-7/mp/mu !Mo/AU3->1/cm3
  real*8,parameter::AU2cm=1.4959787d13 !AU->cm 
contains

  !*************
  !scale height, AU
  function H(r)
    implicit none
    real*8::H,r
    H = H100*(r/1d2)**hh !AU
  end function H

  !*************
  !normalization surface density, Mo/AU2
  function sigmac()
    implicit none
    real*8::sigmac
    sigmac = Mgas*(2.-g)/(2*pi*Rc**2) !Mo/AU2
  end function sigmac

  !*************
  !surface mass density, Mo/AU2
  function sigma(r)
    implicit none
    real*8::sigma,r
    sigma = sigmac()*(r/Rc)**(-g)*exp(-(r/Rc)**(2.-g)) !Mo/AU2
  end function sigma
  
  !*************
  !disk mass density, Mo/AU3
  function rho_disk(r,z)
    implicit none
    real*8::rho_disk,r,z
    rho_disk = sigma(r)/H(r)/pi*exp(-z**2/H(r)**2) !Mo/AU3
  end function rho_disk
  
  !*************
  !number density, 1/cm3
  function ngas_disk(rin,zin)
    implicit none
    real*8::ngas_disk,rin,r,zin,z
    z = abs(zin)
    r = abs(rin)
    ngas_disk = max(min(rho_disk(r,z)*fact3,1d10),1d-10) !cm-3
  end function ngas_disk

  !**************
  !gas temperature, K
  function Tgas_disk(rin,zin)
    implicit none
    real*8::Tgas_disk,rin,r,zin,z
    z = abs(zin)
    r = abs(rin)
    Tgas_disk = min(T0*(r/R0)**(-q)*exp(min(log(beta)*z/H(r),1d2)),1d4) !K
  end function Tgas_disk

  !****************
  !turubulence time-step, s
  function dt_disk(rin,zin)
    implicit none
    real*8::dt_disk,Tgas,ngas,gamma,cs,rin,r,zin,z
    z = abs(zin)
    r = abs(rin)
    gamma = 7./5.
    Tgas = Tgas_disk(r,z)
    ngas = ngas_disk(r,z)
    cs = sqrt(gamma*kb_erg*Tgas/mp/mu) !cm/s
    dt_disk = H(r)/cs*AU2cm !s
  end function dt_disk

end module disk_mods
