#!/bin/bash

echo "This removes ~ file in subdirs, if any."
find . | grep "~"
find . | grep "~" | xargs rm
echo "done!"