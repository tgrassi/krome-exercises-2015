program stroe
  use krome_main
  use krome_user
  implicit none
  integer,parameter::ngrid=300 !grid points
  integer::i,j
  real*8::Tgas,dt,x(krome_nmols),spy,pc,Rsun
  real*8::xall(ngrid,krome_nmols),rmin,rmax
  real*8::Tstar,Rstar,dr,rold,r,t,op(krome_nPhotoBins)

  call krome_init()

  Tgas = 1d4 !gas temperature (K)
  spy = krome_seconds_per_year !s
  pc = 3.085d18 !1 pc in cm
  Rsun = 7d10 !sun radius (cm)
  rmin = pc !min radius (cm)
  rmax = 6.6d3*pc !box radius (cm)
  tend = !!!ADD HERE!!! !end time (s)
  Tstar = !!!ADD HERE!!! temperature (K)
  !!!ADD HERE!!!
  
  dt = !!!ADD HERE!!! !initial time-step
  !time loop
  do
     !!!ADD HERE!!!
     rold = 0d0
     !grid loop
     do i=1,ngrid
        r = (i-1)*(rmax-rmin)/(ngrid-1) + rmin
        dr = r - rold
        rold = r
        !!!ADD HERE!!!
     end do
     if(t>tend) exit
  end do

end program stroe
