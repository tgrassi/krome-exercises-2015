from radmc3dPy import *
import numpy as np
import matplotlib.pyplot as plb

#
# Create the master parameter file for the model 'ppdisk_tutorial'
#
analyze.writeDefaultParfile('ppdisk_tutorial')


#
# Set up the model with the 'default' dust opacity, creating all necessary input files
#
setup.problemSetupDust('ppdisk_tutorial', ny='[10,40,40,10]', ybound='[0, pi/2.-0.5, pi/2., pi/2.+0.5, pi]',
        dustkappa_ext="['MgFeOlivine_Dor95_0p005_0p5']")

import os
os.system('radmc3d mctherm')

#
# Make a safety copy of the dust temperature to be able to compare it to the calculated one for
# other dust opacities
#
os.system('cp dust_temperature.bdat dust_temperature_1.bdat')

#
# Now run it for the other dust opacities 
#
setup.problemSetupDust('ppdisk_tutorial', ny='[10,40,40,10]', ybound='[0, pi/2.-0.5, pi/2., pi/2.+0.5, pi]',
        dustkappa_ext="['MgFeOlivine_Dor95_0p005_1000p0']")

import os
os.system('radmc3d mctherm')

#
# Read the dust opacities
#
o1 = analyze.readOpac(ext=['MgFeOlivine_Dor95_0p005_0p5'])
o2 = analyze.readOpac(ext=['MgFeOlivine_Dor95_0p005_1000p0'])

d1 = analyze.radmc3dData()
d2 = analyze.radmc3dData()
d1.readDustTemp(fname='dust_temperature_1.bdat')
d2.readDustTemp(fname='dust_temperature.bdat')


#
# Plot the dust opacities
#

fig = plb.figure()
plb.loglog(o1.wav[0], o1.kabs[0])
plb.loglog(o2.wav[0], o2.kabs[0], 'r-')
plb.xlabel(r'$\lambda$ [$\mu$m]')
plb.ylabel(r'$\kappa_{\rm abs}$')

#
# Plot the dust temperature as a function of radius in the disk midplane
#
fig = plb.figure()
plb.loglog(d1.grid.x, d1.dusttemp[:,d1.grid.ny/2-1,0,0])
plb.loglog(d2.grid.x, d2.dusttemp[:,d2.grid.ny/2-1,0,0], 'r-')
plb.xlabel('r [AU]')
plb.ylabel('T [K]')


dum = raw_input()



