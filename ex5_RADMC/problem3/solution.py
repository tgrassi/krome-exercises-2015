from radmc3dPy import *
import numpy as np
import matplotlib.pyplot as plb
import os

#
# Create the master parameter file for the model 'ppdisk_tutorial'
#
analyze.writeDefaultParfile('ppdisk_tutorial')
#
# Now set up the model, creating all necessary input files
#
setup.problemSetupDust('ppdisk_tutorial')
#
# Calculate the dust temperature
#
os.system('radmc3d mctherm setthreads 3')
#
# Calculate a 300 by 300 pixel image with 400AU each side and with the disk
#  seen at an inclination angle of 10deg at a wavelength of 1300micron
#
os.system('radmc3d image npix 300 sizeau 400 incl 20. lambda 880.')
# -----------------------------------------------------------------------
# Read the image
# -----------------------------------------------------------------------
im = image.readImage()
fig = plb.figure()
image.plotImage(im, arcsec=True, dpc=140., cmap=plb.cm.afmhot, saturate=0.1)
fig.savefig('problem3_submm_infres.png')

#
# Convolve the image with a 0.3" circular Gaussian kernel to simulate 
# SMA observations
#
cim = im.imConv(fwhm=[0.3, 0.3], pa=0., dpc=140.)
fig = plb.figure()
image.plotImage(cim, arcsec=True, dpc=140., cmap=plb.cm.afmhot)
fig.savefig('problem3_submm_sma.png')

#
# Convolve the image with a 0.05" circular Gaussian kernel to simulate 
# ALMA observations
#
cim = im.imConv(fwhm=[0.07, 0.07], pa=0., dpc=140.)
fig = plb.figure()
image.plotImage(cim, arcsec=True, dpc=140., cmap=plb.cm.afmhot, saturate=0.3)
fig.savefig('problem3_submm_ALMA.png')

os.system('radmc3d image npix 300 sizeau 400 incl 20. lambda 880.')
os.system('cp iamge.out image1.out')
im = image.readImage('image1.out')
image.plotImage(im, arcsec=True, dpc=140.)


o = analyze.readOpac(idust=[0])

ii = abs(o.wav[0]-1.6).argmin()
scatang = np.arange(180.)/179.*np.pi
hg_phasefunc = (1.-o.phase_g[0][ii]) / (1. + o.phase_g[0][ii]**2 - 2.*o.phase_g[0][ii]*np.cos(scatang))**(3./2.)

plb.plot(scatang/np.pi*180., hg_phasefunc)
plb.yscale('log')
plb.xlabel('Scattering angle [deg]')
plb.ylabel('Phase function')


dum = raw_input()


