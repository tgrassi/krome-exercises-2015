from radmc3dPy import *
import numpy as np
import matplotlib.pyplot as plb

#
# Create the master parameter file for the model 'ppdisk_tutorial'
#
analyze.writeDefaultParfile('ppdisk_tutorial')
#
# Now set up the model, creating all necessary input files
#
setup.problemSetupDust('ppdisk_tutorial')
#
# Read the dust density
#
data = analyze.readData(ddens=True, dtemp=True)
#
# Calculate the optical depth
#
data.getTau(wav=0.93)

# -----------------------------------------------------------------------
# Plot the optical depth
# -----------------------------------------------------------------------
#
# Plot the radial optical depth (as seen from the star) as a function of the spherical radial coordinate
#
fig = plb.figure()
plb.loglog(data.grid.x/natconst.au, data.taux[:,data.grid.ny/2-1, 0], 'ko-')
plb.xlabel('r [AU]')
plb.ylabel(r'$\tau_{\rm r}$')

#
# Plot the radial optical depth (as seen from the star) as a function of the polar angle at an arbitrary radius
#
ir = 40
fig = plb.figure()
plb.plot(np.pi/2.-data.grid.y, data.taux[ir,:, 0], 'ko-')
plb.xlabel(r'$\theta$ [rad]')
plb.ylabel(r'$\tau_{\rm r}$')
plb.yscale('log')

fig = plb.figure()
plb.plot(np.pi/2.-data.grid.y, data.dusttemp[ir,:, 0], 'ko-')
plb.xlabel(r'$\theta$ [rad]')
plb.ylabel('T [K]')

# -----------------------------------------------------------------------
# Plot the temperature
# -----------------------------------------------------------------------
#
# Make a 2D contour of the dust density
#
fig = plb.figure()

c = plb.contourf(data.grid.x/natconst.au, np.pi/2.-data.grid.y, data.dusttemp[:,:,0,0].T, 30)
plb.xlabel('r [AU]')
plb.ylabel(r'$\pi/2-\theta$')
plb.xscale('log')

cb = plb.colorbar(c)
cb.set_label(r'$\log_{10}{\rho}$', rotation=270., labelpad=20)

# Overplot the optical depth of unity line
plb.contour(data.grid.x/natconst.au, np.pi/2.-data.grid.y, data.taux[:,:,0].T, [1.0], colors='w')



dum = raw_input()


