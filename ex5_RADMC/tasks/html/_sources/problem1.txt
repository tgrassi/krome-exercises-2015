.. _problem1:

**********************
Problem 1 
**********************

Purpose of this exercise
========================

The purpose of this exercise is to familiarize yourself with 

* How to create / set up a model from scratch

* How to read variables (e.g. density, opacity) and create diagnostic plots 

Part A
======

Go to the problem1 directory. The directory contains a dust opacity table (dustkappa_MgFeOlivine_Dor95_0p005_0p5.inp)
and a file with a protoplanetary disk model (ppdisk_tutorial.py). Use this model and create the master parameter file
(problem_params.inp) of the model with the default parameters using the :meth:`analyze.writeDefaultParams()` function.
Then set up the model and create all necessary input files with the :meth:`setup.problemSetupDust()` function.


Part B
======
Read the dust opacity and plot it. You can use e.g. the :meth:`analyze.readOpac()` function. When the 
:meth:`analyze.readOpac()` is called you either have to pass the index of the dust species you wish to read
with the idust parameter (i.e. idust=[0] if only one dust species is present) or you have to pass the 'extension' of the
dust opacity file name. This latter is the part of the dust opacity file name between the 'dustkappa_' and the '.inp'. 

The returning value of the :meth:`analyze.readOpac()` is an instance of the :class:`radmc3dDustOpac` class.
Note, that each attribute/variable of this class is a list with as many elements as the number of dust species read. I.e. 
the wavelength grid vector of the first dust opacity is :attr:`radmc3dDustOpac.wav[0]` while the corresponding mass absorption
coefficient is :attr:`radmc3dDustOpac.kabs[0]`. 

The end result should look like this:

.. image:: screenshots/problem1_dustopac_plot.png
    :align: center


Part C
======
Read the dust density with the :meth:`analyze.readData()` function. This function will return an instance of the
:class:`radmc3dData` class with the dust density contained in :attr:`radmc3dData.rhodust`. 
Calculate the optical depth at 0.93 :math:`\mu{\rm m}` with the :meth:`radmc3dData.getTau()` function. Note, that you have to pass the wavelength at
which you wish to calculate the optical depth with the 'wav' keyword, that takes the wavelength in :math:`\mu{\rm m}`.
The radial and poloidal (vertical) optical depths will be in the :attr:`radmc3dData.taux` 
and :attr:`radmc3dData.tauy`
variables. 

Now make a 2D contour plot of the logarithm of the density. Try to overplot the radial optical depth of unity contour. 
The end result should look like this:

.. image:: screenshots/problem1_2dcontours.png
    :align: center

Plot the radial optical depth as a function of the poloidal (i.e. vertical) angular coordiante. 

.. image:: screenshots/problem1_tau_vertical.png
    :align: center
